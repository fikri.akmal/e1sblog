// Display all Comment with ajax
$(document).ready(function() {
	// const pk = window.location.href.split("/")[5].split("?")[0];
    const pk = window.location.href.split("/")[4]
	$.ajax({
		url:'/blog/getAllComment/' + pk,
		success: (e) => {
			for (let i = e.length-1; i>=0; i--) {
				$(".list-komen").append(
					`<div class="row d-flex justify-content-center">
						<div class="col-md-8 nama-komentar">
							<span style="z-index:99;" class="spanNama" id=${e[i].pk}>${e[i].fields.nama}</span>
						</div>
					</div>
					
					<div class="row d-flex justify-content-center">
						<div class="col-md-8 komentar-orang">
							${e[i].fields.post_date}<br><br>
							${e[i].fields.komentar}
						</div>
					</div>`
				);
			}
		}
	})
})

// Comment AJAX
$(".postComment").on('submit', (e) => {
	e.preventDefault();
	const pk = window.location.href.split("/")[4]
	const nama = $("#nama").val();
	const komentar = $("#komentar").val();
	console.log($("input[name=csrfmiddlewaretoken]").attr("value"));

	if (nama !='' && komentar != '') {
	$.ajax({
		url:'/blog/postComment/' + pk,
		type:"POST",
		data: {
			'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
			'nama':nama,
			'komentar': komentar,
			'pk':pk
		},
		success: (e) => {
			$("#nama").val("");
			$("#komentar").val("");
			$(".list-komen").empty();
			
			for (let i=e.length-1; i>=0; i--) {
				$(".list-komen").append(
					`<div class="row d-flex justify-content-center">
						<div class="col-md-8 nama-komentar">
							<span style="z-index:99;" class="spanNama" id=${e[i].pk}>${e[i].fields.nama}</span>
						</div>
					</div>
					
					<div class="row d-flex justify-content-center">
						<div class="col-md-8 komentar-orang">
							${e[i].fields.post_date}<br><br>
							${e[i].fields.komentar}
						</div>
					</div>`
				)
			}
		}
	})
}
})