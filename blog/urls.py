from django.urls import path
from .views import home, addPost, blogDetails, EditPost, postComment, getAllComment, deletePost

urlpatterns = [
    path('', home, name="home"),
    path('addPost', addPost, name="addPost"),
    path('blog/<int:pk>/', blogDetails, name="blogDetails"),
    path('blog/edit/<int:pk>/', EditPost.as_view(), name="editPost"),
    path('blog/postComment/<int:pk>', postComment, name="postComment"),
    path('blog/getAllComment/<int:pk>', getAllComment, name="getAllComment"),
    path('blog/delete/<int:pk>', deletePost, name="deletePost")
]