from django import forms
from .models import BlogPost
from ckeditor.widgets import CKEditorWidget

class addForm(forms.ModelForm):
    title = forms.CharField(required=True)
    body = forms.CharField(required=True, widget=CKEditorWidget())
    snippet = forms.CharField(required=True)
    title.widget.attrs.update({'class':'form-control', 'style':'width:60%'})
    body.widget.attrs.update({'class':'form-control', 'style':'width:100%'})
    snippet.widget.attrs.update({'class':'form-control', 'style':'width:60%'})

    class Meta:
        model = BlogPost
        fields = ['title', 'snippet', 'body']


