from django.shortcuts import render, redirect
from .models import BlogPost, Comment
from .forms import addForm
from django.views.generic import UpdateView
from django.core import serializers
from django.http import HttpResponse
# Create your views here.

def home(request):
    blogPost = BlogPost.objects.all()
    response = {'blogPost': blogPost}

    return render(request, "home.html", response)


def addPost(request):
    context = {
        "form": addForm()
    }

    if request.method == "POST":
        form = addForm(request.POST)
        context['posted'] = form.instance
        if form.is_valid():
            instance = form.save(commit=False)
            instance.author = request.user
            instance.save()
            return redirect('home')

    return render(request, 'addPost.html', context)

def blogDetails(request, pk):
    blog = BlogPost.objects.get(id=pk)
    editAccess = (request.user == blog.author)
    context = {
        'blog':blog,
        'editAccess':editAccess
    }

    return render(request, 'blogDetails.html', context)


def getAllComment(request,pk):
	blog = BlogPost.objects.get(id=int(pk))
	comment = Comment.objects.filter(blog=blog)
	data = serializers.serialize("json", comment)
	return HttpResponse(data, content_type="text/json-comment-filtered")


def postComment(request, pk):
	if request.method == "POST":
		nama = request.POST.get('nama')
		komentar = request.POST.get('komentar')
		blog_obj = BlogPost.objects.get(id=int(pk))

		comment = Comment.objects.create(nama=nama, komentar=komentar, blog=blog_obj)
		comment.save()
		comments = Comment.objects.filter(blog=blog_obj)
		comment_data = serializers.serialize("json", comments)
		return HttpResponse(comment_data, content_type="text/json-comment-filtered")

def deletePost(request, pk):
    blog = BlogPost.objects.get(id = pk)
    
    if request.method == "POST":
        blog.delete()
        return redirect("home")

    context = {
        'blog': blog
    }

    return render(request, "deletePost.html", context)


class EditPost(UpdateView):
    model = BlogPost
    template_name = 'editPost.html'
    form_class = addForm