from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.

class BlogPost(models.Model):
    author = models.ForeignKey(User, default = None, on_delete=models.PROTECT, db_constraint=False)
    title = models.CharField(max_length=100, blank=False)
    post_date = models.DateField(auto_now_add=True)
    body = RichTextField(blank=False, null=False)
    snippet = models.CharField(blank=False, null=False, max_length=100)

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('home')

class Comment(models.Model):
    komentar = models.CharField(max_length=200, null=False, blank=False)
    nama = models.CharField(max_length=30)
    blog = models.ForeignKey(BlogPost, on_delete=models.CASCADE)
    post_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.nama

