from django.shortcuts import render, redirect
from django.contrib.auth import logout
from django.contrib.auth.models import auth, User
from django.contrib import messages
# Create your views here.


def register(request):
    if request.method == "POST":
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')
        password_conf = request.POST.get('password1')

        valid = User.objects.filter(username=username)

        if (password != password_conf):
            messages.warning(request, "Password does not match")

        elif valid.exists():
            messages.warning(request, "Username already existed")

        else:
            newUser = User.objects.create_user(username = username, email = email, password = password)
            newUser.save()
            newUser = auth.authenticate(username=username, password=password)
            auth.login(request, newUser)
            return redirect('home')

    return render(request, 'register.html')


def login(request):
    if request.user.is_authenticated:
        return redirect('home')

    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = auth.authenticate(username = username, password = password)
        
        if user is not None:
            auth.login(request,user)
            return redirect('home')

        else:
            messages.warning(request, "Wrong login info!")

    return render(request, 'login.html')


def log_out(request):
    logout(request)
    return redirect('home')


